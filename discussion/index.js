// console.log("Array Methods");

// Array Methods

/*
	1. Mutator Methods
		- it seeks to modify the contents of an array
		- are functions that mutate an array after they are created
		- These methods manipulate original array peroforming various tasks such as adding or removing elements.
*/

let fruits = ["Apple", "Orange", "Kiwi", "Watermelon"]

/*
	push()
		- adds an element in the end of an array and returns the array's length

	Syntax:
		arrayName.push(element)	
*/

console.log("Current Fruits Array:")
console.log(fruits);

// Adding elements

fruits.push("Mango")
console.log(fruits)

let fruitsLength = fruits.push("Melon")
console.log(fruitsLength)
console.log(fruits)

fruits.push("Avocado", "Guava")
console.log(fruits)


// Pop Method
/*
	pop()
		- it removes the last element in our rray and returns the removed element (when applied inside a variable)

	Syntax:
		arrayName.pop()	
*/

let removedFruit = fruits.pop() // remove last element
console.log(removedFruit)
console.log("Mutated Array from the Pop Method:")
console.log(fruits)


// Unshift Method
/*
	unshift()
		- adds 1 or more elements at the beginning of an array
		- returns the length of the array (when presented inside a variable)

	Syntax:
		arrayName.unshift(elementA)	
		arrayName.unshift(elementA, elementB)
*/

fruits.unshift("Lime", "Banana")
console.log("Mutated array from the Unshift Method:")
console.log(fruits)


// Shift Method
/*
	shift()
		- it removes an element at the beginning of an array and returns the removed element

	Syntax:
		arrayName.shift()	
*/

let removedFruit2 = fruits.shift()
console.log(removedFruit2)
console.log("Mutated array from the Shift Method:")
console.log(fruits);


// Splice Method
/*
	splice()
		- allows to simultaneously remove elements from a specified index number and adds an element

	Syntax:
		arrayName.splice(startingIndex, deleteCount, elementsToBe Added)
*/

let fruitsSplice = fruits.splice(1, 2, "Cherry", "Lychee")
console.log("Mutated arrays from Splice Method:")
console.log(fruits)
console.log(fruitsSplice)

// Deleting using splice without adding elements
let removedSplice = fruits.splice(3, 2)
console.log(fruits)
console.log(removedSplice)



// Sort Method
/*
	sort()
		- it rearranges the array element in alphanumeric order

	Syntax:
		arrayName.sort()	
*/

// fruits.sort()
// console.log("Mutated array from Sort Method:")
// console.log(fruits)

let mixedArr = [12, "May", 36, 94, "August", 5, 6.3, "September", 10, 100, 1000]
console.log(mixedArr.sort());


// Reverse Method
/*
	reverse()
		- it reverses the order or the element in an array

	Syntax:
		arrayName.reverse();	
*/

fruits.reverse()
console.log("Mutated array from Reverse Method:")
console.log(fruits)


// For sorting the items in descending order:

 	fruits.sort().reverse()
 	console.log(fruits)

/*
 	MINI ACTIVITY 1:
	 - Debug the function which will allow us to list fruits in the fruits array.
	 	-- this function should be able to receive a string.
	 	-- determine if the input fruit name already exist in the fruits array.
	 		*** If it does, show an alert message: "Fruit already listed on our inventory".
	 		*** If not, add the new fruit into the fruits array ans show an alert message: "Fruit is not listed in our inventory."
	 	-- invoke and register a new fruit in the fruit array.
	 	-- log the updated fruits array in the console*/


	function registerFruit (fruitName) {
	 	let doesFruitExist = fruits.includes(fruitName); // checks if already include

	 	if(doesFruitExist) {
	 			alert(fruitName + " is already on our inventory")
	 	} else {
	 		fruits.push(fruitName);
	 	
	 		alert("Fruit is now listed in our inventory")
	 		}
	 	}
	 	
	let newFruit = registerFruit("Blueberry") 
	console.log("Here are the updated list of fruits:")	
	console.log(fruits)	


// MINI ACTIVITY 2 - ARRAY MUTATORS

/*
	- Create an empty array
	- use the push() method and unshift() method to add elements in your array
	- use the pop() and shift() method to remove elements in your array
	- use sort to organize your array in alphanumeric order
	- at the end the array should contain not less than 6 elements after applying these methods.
	- log the changes in the console every after update
*/

let pokemon = ["Pikachu", "Squirtle", "Charmander", "Bulbasaur"]

console.log("Current Pokemon:")
console.log(pokemon);

pokemon.push("Togepi")
console.log(pokemon)

pokemon.unshift("Gengar", "Chikorita", "Charizard")
console.log("Pokemon Unshift Method:")
console.log(pokemon)

let removedPokemon = pokemon.pop() 
console.log(removedPokemon)
console.log("Pokemon Pop Method:")
console.log(pokemon)

let removedPokemon2 = pokemon.shift()
console.log(removedPokemon2)
console.log("Pokemon Shift Method:")
console.log(pokemon);

pokemon.sort()
console.log("Pokemon Sort Method:")
console.log(pokemon)


// NON-MUTATOR METHODS

/*
	Non-mutator Methods
		- these are functions or methods that do not modify or change an array after they are created
		- these methods do not manipulate the original array performaing various tasks such as returning elements from an array and combining arrays and printing the output
*/
console.log(" ")
console.log("NON-MUTATOR METHODS")
let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"]

console.log(countries);

// IndexOf Method
/*
	indexOf()
		- returns the index number of the first matching element found in an array. If no match as found, the result will be -1. 
		- the search process will be done from our first element proceeeding to the last element

	Syntax:
		arrayName.indexOf(searchValue) //
		arrayName.index.Of(searhValue, fromIndex)	
*/

let firstIndex = countries.indexOf("PH")
console.log("Result of indexOf method: " + firstIndex) // result: 1

firstIndex = countries.indexOf("PH", 4)
console.log("Result of indexOf method: " + firstIndex)

firstIndex = countries.indexOf("PH", 7)
console.log("Result of indexOf method: " + firstIndex)

firstIndex = countries.indexOf("PH", -1)
console.log("Result of indexOf method: " + firstIndex)

console.log(" ")


// Last IndexOf Method
/*
	lastIndexOf()
		- returns the index number of the last element found in an array. The search process will be done from the last element proceeding to the first

	Syntax:
		arrayName.lastIndexOf(searchValue)
		arrayName.lastIndexOf(searchValue, fromIndex)	
*/

let lastIndex = countries.lastIndexOf("PH")
console.log("Result of lastIndexOf method: " + lastIndex)

lastIndex = countries.lastIndexOf("PH", 4)
console.log("Result of lastIndexOf method: " + lastIndex)


// Slice Method
/*
	slice()
		- portions or slices the elements from our array and returns a new array

	Syntax:
		arrayName.slice(startingIndex)	
		arrayName.slice(startingIndex, endingIndex)	
*/

console.log(countries)

let sliceArrA = countries.slice(2)
console.log("Result from Slice Method:")
console.log(sliceArrA)
console.log(countries)

let sliceArrB = countries.slice(2, 5)
console.log("Result from Slice Method:")
console.log(sliceArrB)
console.log(countries)

let sliceArrC = countries.slice(-3)
console.log("Result from Slice Method:")
console.log(sliceArrC)
console.log(countries)



// ToString Method
/*
	toString()
		- returns an arrays as a string separated by commas
		- it is used initernally by javascript when an array needs to be displayed as a text(like in HTML), or when an object or array needs to be use as a string.

	Syntax:
		arrayName.toString()	
*/

let stringArray = countries.toString() // will be print as a text
console.log("Result from toString Method:")
console.log(stringArray)

let mixedArrToString = mixedArr.toString()
console.log(mixedArrToString)


// Concat Method
/*
	concat()
		- combines 2 or more arrays and returns the combined result

	Syntax:
		arrayA.concat(arrayB)
		arrayA.concat(elementA)	
*/

let taskArrayA = ["drink HTML", "eat Javascript"]
let taskArrayB = ["inhale CSS", "breath sass"]
let taskArrayC = ["get git", "be node"]

let tasks = taskArrayA.concat(taskArrayB)
console.log("Result from concat method:")
console.log(tasks)

let allTasks = taskArrayA.concat(taskArrayB, taskArrayC)
console.log("Result of All Tasks:")
console.log(allTasks)

// Combining arrays with elements - similar with push()
let combineTask = taskArrayA.concat("smell expres", "throw react")
console.log(combineTask)
// doesn't change the array, but only console the output


// Join Method
/*
	join()
		- returns an array as a string
		- does not change the original array
		- we can use any separator. The default is comma (,)

	Syntax:
		arrayName.join(separatorString)
*/

let students = ["Elysha", "Gab", "Ronel", "Jean"]
console.log(students.join())

console.log(students.join(' '))
console.log(students.join(" - "))



// ITERATION METHODS

/*
	Iteration Methods
		- are loops designed to perform repitive tasks in array. 
		- used for manipulation array data resulting in complex tasks
		- normally work with a function supplied as an argument
		- it aims to evaluate each element in an array
*/

// For Each
/*
	forEach()
		- similar to for loop that iterates on each array element

	Syntax:
		arrayName.forEach(function(individualElement) {
			statement
		})	
*/

allTasks.forEach(function(task) {
	console.log(task)
})


// Using forEach with conditional statements

console.log(" ")
let filteredTasks = []

allTasks.forEach(function(task) {
	console.log(task)
	if(task.length > 10) {
		filteredTasks.push(task)
	}
})

console.log(allTasks)
console.log("Result of filteredTasks: ")
console.log(filteredTasks);


// Map Method
/*
	map()
		- iterates on each element and returns a new array with different values depending on the result of the function's operation

	Syntax:
		arrayName.map(function(individualElement) {
			statement
		})	
*/

let numbers = [1, 2, 3, 4, 5]

let numberMap = numbers.map(function(number) {
	console.log(number)
	return number*number
})

console.log("Original Array:")
console.log(numbers)
console.log("Result of Map MethodL:")
console.log(numberMap)


// Every Method

/*
	every()
		- checks if all elements in an array met the given condition. Returns a true value if all elements meet the condition and falls if otherwise

	Syntax:
		arrayName.every(function(individualElement) {
			return expression/condition
		})	
*/

let allValid = numbers.every(function(number) {
	return (number < 3);
})

console.log("Result of every method:")
console.log(allValid)


// Some Method
/*
	some()
		- checks if at least one element in the array meet the given condition. Returns a true value if at least one of the elements meets the given condition and it returns false if otherwise

	Syntax:
		arrayName.some(function(individualElement) {
			return expression/condition
		})	
*/

let someValid = numbers.some(function(number) {
	return (number < 3)
})

console.log("Result of some method:")
console.log(someValid)


// Filtered Method
/*
	filter()
		- returns a new array that contains elements which meets the given condition. Return an empty array in no elements were found (that satisfied the given condition)

	Syntax:
		arrayName.filter(function(individualElement) {
			return expression/ condition
		})	
*/

let filteredValid = numbers.filter(function(number) {
	return (number < 3)
})

console.log("Result of filter method:")
console.log(filteredValid)


// Include Method

/*
	includes()
		- checks if the argument passed can be found in an array
		- can be chained after another method. The result of the first method is used on the second method until all chained methods have been resolved.
			
*/

let products = ["mouse", "KEYBOARD", "laptop", "monitor"]

let filteredProducts = products.filter(function(product) {
	return product.toLowerCase().includes("a")
})

console.log("Result of includes method:")
console.log(filteredProducts)